#!/usr/bin/env stack
-- stack --resolver lts-12.5 script

{-

 compile with `stack ghc anki.hs`  

 convert to html:
 `pandoc -f markdown -t html --filter anki test.md | iconv -f UTF-8 -t UNICODE > test.html`

 convert to latex
 `pandoc -f markdown -t latex -s --template template.tex --filter anki test.md > test.tex`
 `pdflatex test.tex`

-}


import Text.Pandoc.JSON
import Text.Pandoc.Walk
import SimpleAnkiParser (handleAnki)

main :: IO ()
main = toJSONFilter (handleAnki '_')
