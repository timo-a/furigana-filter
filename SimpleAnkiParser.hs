#!/usr/bin/env stack

module SimpleAnkiParser (lexer,
                         parse,
                         group,
                         Interlingua(..),
                         handleAnki
                        )
  where

import Text.Pandoc (Format(..), Inline(RawInline, Str))
import Data.List hiding (group)
import Data.List.Split

handleAnki :: Char -> Maybe Format -> Inline -> Inline
handleAnki pseudoSpace (Just format) (Str s) = case format of 
   Format "html"  -> (RawInline (Format "html" ) . toHTML  . group . parse . lexer pseudoSpace) s
   Format "latex" -> (RawInline (Format "latex") . toXetex . group . parse . texlexer pseudoSpace) s
handleAnki _ _ x = x

data Lexed = Letter Char | Space | BracketL | BracketR deriving (Eq, Show)
lexed2Char :: Lexed -> Char 
lexed2Char (Letter c) = c
lexed2Char Space      = ' '
lexed2Char BracketL   = '['
lexed2Char BracketR   = ']'

--preprocesses strings by identifying kind of character they are and handles escaping pseudospace
lexer :: Char -> String -> [Lexed]
lexer pseudoSpace = h
   where
     h (' ':ss)              = Space             :h ss -- this is not going to happen because pandoc splits on those but just in case
     h (s:ss) | s==pseudoSpace = Space             :h ss
     h ('§':s:ss) | s==pseudoSpace = Letter pseudoSpace:h ss
     h ('[':ss)              = BracketL          :h ss
     h (']':ss)              = BracketR          :h ss
     h (s:ss)                = Letter s          :h ss
     h []                    = []

texlexer :: Char -> String -> [Lexed] --latex can't deal with `_` in the text so we need to escape it with `\`
texlexer pseudoSpace = h
   where
     h (' ':ss)              = Space             :h ss -- this is not going to happen because pandoc splits on those but just in case
     h (s:ss) | s==pseudoSpace = Space             :h ss
     h ('§':s:ss) | s==pseudoSpace = Letter '\\' : Letter pseudoSpace:h ss
     h ('[':ss)              = BracketL          :h ss
     h (']':ss)              = BracketR          :h ss
     h (s:ss)                = Letter s          :h ss
     h []                    = []


data Part = BottomText String
          | Uptext String
          | Blank
          deriving (Show, Eq)

data Interlingua = Kana String
                 | Furigana String String
                 deriving (Show, Eq)

--convert Lexed characters to tags 
parse :: [Lexed] -> [Part]
parse lss = case break (`elem` [BracketL,Space]) lss of
     ([],   [])          -> []
     ([],   BracketL:ls) -> parseUpText ls
     ([],   Space:ls)    -> Blank     : parse ls
     (kana, ls)          -> BottomText (mc kana) : parse ls
     where
       mc = map lexed2Char

parseUpText :: [Lexed] -> [Part]
parseUpText lss = case break (==BracketR) lss of
      ([],              []) -> []
      (kana,            []) -> [BottomText ('[':mc kana)]
      ([],     BracketR:ls) -> parse ls
      (uptext, BracketR:ls) -> Uptext (mc uptext) : parse ls
      where
       mc  = map lexed2Char
        

--group patterns of Parts
group :: [Part] -> [Interlingua]
group (Blank : Blank : ms) = group $ Blank : ms
--group (Blank : Blank : BottomText kj : Uptext ka : ms) = group $ Blank : BottomText kj : Uptext ka : ms
group           (BottomText kj : Uptext ka : ms) = Furigana kj ka : group ms
group (Blank:ms)        = Kana " ": group ms
group (BottomText ki : Blank : BottomText kj : Uptext ka : ms) = Kana ki : group (BottomText kj : Uptext ka : ms)
group (BottomText s:ms) = Kana s: group ms
group (Uptext ka:ms)    = Kana ('[':ka++"]"): group ms -- uptext without preceeding bottomtext -> '[',']' are regular characters
group [] = []


toFormat :: (String, String, String) -> [Interlingua] -> String
toFormat (start, middle, end) =  concatMap m2s
     where
        m2s (Furigana below above) = start ++ below ++ middle ++ above ++ end
        m2s (Kana k) = k


toHTML ::  [Interlingua] -> String
toHTML = toFormat ("<ruby><rb>", "<rt>", "</ruby>")

toXetex ::  [Interlingua] -> String
toXetex = toFormat ("\\ruby{", "}{", "}")
