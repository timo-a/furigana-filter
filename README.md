# anki converter

For a more detailed explanation see examples/selfcontainedexample.
Pandoc filter that converts anki style furigana (which is comparatively convenient to type) to either html of latex.

Compile with

    stack ghc anki_.hs

then for converting to html:

    pandoc -f markdown -t html --filter anki_ test.md | iconv -f UTF-8 -t UNICODE > test.html

or, for converting to latex:

    pandoc -f markdown -t latex -s --template template.tex --filter anki_ test.md > test.tex
    pdflatex test.tex


So far the format is not a perfect match of the one offered by anki.
It may change in future versions.

E.g. In Anki `私は 食[たべ]べたい` sets `たべ` over `食` without printing the space. As pandoc uses space to split words we need to use a differen character such as underscore here: `私は_食[たべ]べたい`
