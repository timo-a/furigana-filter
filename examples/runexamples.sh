#run selfcontained example
dir=selfcontained
pandoc -f markdown -t html -s --metadata pagetitle="Selfcontained Example" --css ../style.css \
       --filter ../anki_ \
   $dir/selfcontainedexample.md -o $dir/selfcontainedexample.html

pandoc -f markdown -t latex --template template_for_pandoc2.7.3_extended.latex --pdf-engine=xelatex \
   -V mainfont='Noto Serif CJK JP' -V monofont='Noto Sans Mono CJK JP' --filter ../anki_ \
   $dir/selfcontainedexample.md -o $dir/selfcontainedexample.tex

xelatex -output-directory=$dir $dir/selfcontainedexample.tex


#sakura
dir=sakura
pandoc -f markdown -t html -s --metadata pagetitle="Sakura" --css ../style.css \
       --filter ../anki_ \
   $dir/sakura.md -o $dir/sakura.html

pandoc -f markdown -t latex --template template_for_pandoc2.7.3_extended.latex --pdf-engine=xelatex \
   -V mainfont='Noto Serif CJK JP' -V monofont='Noto Sans Mono CJK JP' --filter ../anki_ \
   $dir/sakura.md -o $dir/sakura.tex

xelatex -output-directory=$dir $dir/sakura.tex


