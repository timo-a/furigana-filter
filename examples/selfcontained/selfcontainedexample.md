# furigana-filter turns `振[ふ]り_仮名[がな]` into 振[ふ]り_仮名[がな]!

    compile to html:
    pandoc -f markdown -t html -s --metadata pagetitle="Selfcontained Example" --css ../style.css \
           --filter ../../anki_ \
	   selfcontainedexample.md -o selfcontainedexample.html

    compile to xetex:
     
    pandoc -f markdown -t latex --template ../template_for_pandoc2.7.3_extended.latex --pdf-engine=xelatex \
	   -V mainfont='Noto Serif CJK JP' -V monofont='Noto Sans Mono CJK JP' --filter ../../anki_ \
           selfcontainedexample.md -o selfcontainedexample.tex

    xelatex selfcontainedexample.tex

    template_for_pandoc2.7.3_extended is https://github.com/jgm/pandoc-templates/blob/db59a5e77b0a5629f0801eb82103814842f2e2ed/default.latex but modified by adding
    \usepackage{CJK}
    \usepackage[CJK, overlap]{ruby}
    \usepackage[portrait, margin=6mm]{geometry}
    \usepackage{setspace}
    \linespread{1.3}
    \setlength\parindent{0pt}
    \renewcommand{\rubysize}{0.5}

    before \begin{document}
    

## Furigana

Japanese has ~2000 complicated characters called *kanji* and ~50 characters called *kana* that people can be expected to memorise. There are often several ways to read a single *kanji* but all of them can be expressed in *kana*. So in order to help people who don't know how to read all *kanji*
a sequence of *kana* is written above them e.g. ご_飯[はん]を_食[た]べる
This is called *furigana*.
With *furigana* the whole sequence can be read without knowing a single *kanji*!
In the following I will call the text that has furigana written over it *bottomtext* and the text thet is written over somthing *uptext*.


## The anki furigana format

The flash card app [anki](https://apps.ankiweb.net) offers an ingenious way to write down furigana.
Every character sincle the last `space` or beginning of the line has everything between square brackets written over them.  
So for ご_飯[はん]を_食[た]べる we could just write `ご 飯[はん]を 食[た]べる`. Note the `whitespace`s! Without them it would look like this: ご飯[はん]を食[た]べる

Also note that the `whitespace`s don't show up in the output. If you want to have whitespace in the output you just use two spaces: `ご 飯[はん]を  食[た]べる` gives ご_飯[はん]を__食[た]べる.  

Why do I think this notation is ingenious? You only need 3 characters!  
Compare this to other notation systems:

 - latex: `ご\ruby{飯}{はん}を\ruby{食}{た}べる`
 - html:  `ご<ruby>飯<rt>はん</ruby>を<ruby>食<rt>た</ruby>べる`
 - reddit: `ご[飯](#fg "はん")を[食](#fg "た")べる`
 - japanese.stackexchange: `ご[飯]{はん}を[食]{た}べる` of `ご飯を食べる{ごはんたべる}`


Of course there are different motivations behind the formats. Latex and HTML want to be able to use the established syntax for new functionality and the `stackexchange` one is concernd with whole words being found by search engines.  I on the other hand favour being able wo write it down fast, and readability. Therefore I prefer the anki-format.  

Lets look at some more examples of the anki furigana format:

| anki         | output       | comment                                                              |   
|:------------:|:------------:|----------------------------------------------------------------------|
| `a[1]`          | a[1]         |                                                                          |   
| `a[1]bc[2]`     | a[1]bc[2]    | bottomtext is accumulated starting with b, after the last uptext '1'     |
| `a[1]b c[2]`    | a[1]b_c[2]   | `space` resets bottomtext                                                |   
| `a[1]b  c[2]`   | a[1]b__c[2]  | doubles `space` resets bottomtext and separates words                    |
| `a[1]b    c[2]` | a[1]b___c[2] | >2 `space` are collapsed to 2 `space`s                                   |   
| `a[1] bc[2]`    | a[1] bc[2]   | since `]` already reset bottomtext a single `space` is displayed as such |   
| `a[1 2]b`       | a[1_2]b      | uptext can contain spaces                                                |   
| `a[b[c]d`       | a[b[c]d      | the first `[` encloses everythng until a closing bracket is found        |
| `a[bcd`         | a[bcd        | a bracket that is never closed is displayed                              |

## Enabling Anki format in pandoc

([Pandoc](https://pandoc.org/) is a document converter that let's you e.g. write in Markdown and then compile to latex, that can be extended with *filters*)  
Anki format cannot easily be realised perfectly in pandoc because pandocs splits words on an arbitrary number of whitespace.
So if we write a pandoc filter, we won't be able to process `a[1 2]b` but only `a[1` and `2]b` at different times. Or both elements as part of a list. But even as part of a list we don't know if `['a', 'b[c]']` was originally separated by one or two spaces which would result in either a_b[c]  or a _b[c].

This repository offers 2 pandoc filters that try to approximate anki format: `anki.hs` and `anki_.hs`.
Both offer a pseudo space character that won't trigger splitting by pandoc but will be treated as space by the filter.

In `anki.hs` the special character is a [non-breaking space](https://en.wikipedia.org/wiki/Non-breaking_space) (U+00A0).

`anki_.hs` uses an underscore, in place of whitespace (because nonbreaking space needs to be escaped so it's 2 characters to type). What if you want to use underscore as underscore? You escape it with `§`. For `_` type `§_`, for `§` type `§`, for `§_` type `§§_`. Pandoc already escapes certain characters on `\` we use `§` instead.
I recommend this filter because I find it better to look at and shorter to type.  


Some examples:

|outcome | anki | anki§_hs | comment |
|:---:|:---:|:---:|:------------------------------|
| a b         | `a b`     |  `a b`     ||
|             |           |  `a_b`     ||
| a[b]        | `a[b]`    |  `a[b]`    ||
| a_b[c]     | `a b[1]` |  `a_b[c]`  | in anki space indicates that `a` is not to be superscribed. anki§_ uses an underscore|
| a__b[c]     | `a  b[1]` |  `a b[c]`  | in anki 2 spaces to keep `a` from being superscribed and to separate it from b. In anki§_ 2 `_` are used or, space splits into different words. |
|             |             |  `a__b[c]`  ||
| a[0]_b[1] | `a[0] b[1]` | `a[0] b[1]` ||
|           |             | `a[0]_b[1]` ||
| a[0_1]    | `a[0 1]`    | `a[0_1]`    | Since pandoc splits on every `space` we must substitute space even in the superscript. |
|           |             |             ||
| a§_a        | `a_a`          | `a§_a`     | Regular characters in anki, but anki§_ needs to escape `_` with `§`. |
| a§a         | `a§a`          | `a§a`      | |
| a§_§_a      | `a__a`         | `a§_§_a`   | |
| a§_§a       | `a_§a`         | `a§_§a`    | |
| a§§_a       | `a§_a`         | `a§§_a`    | |
| a§§a        | `a§§a`         | `a§§a`     | |
