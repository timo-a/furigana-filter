#!/usr/bin/env stack
-- stack --resolver lts-12.5 runghc --package HUnit
--module TestAnki_ where

import Test.HUnit
import SimpleAnkiParser


main :: IO Counts
main = runTestTT $ TestList [ TestLabel test1_label test1
                            , TestLabel test2_label test2
                            , TestLabel test3_label test3
                            , TestLabel test4_label test4
                            , TestLabel test5_label test5
                            ]
       
lex2group_ = group . parse . lexer '_'

test1_label = "Space between regular and furigana should not be visible"
test1 :: Test
test1 = TestCase $ assertEqual "for (\"a_b[1]\")"
                               [Kana "a", Furigana "b" "1"]
                               (lex2group_ "a_b[1]")

test2_label = "2 spaces between regular and furigana become one"
test2 :: Test
test2 = TestCase $ assertEqual "for (\"a__b[1]\")"
                               [Kana "a", Kana " ", Furigana "b" "1"]
                               (lex2group_ "a__b[1]")

test3_label = "3 or more spaces get collapsed to 1"
test3 :: Test
test3 = TestCase $ assertEqual "for (\"a__+b[1]\")"
                               (take 5 $ repeat [Kana "a", Kana " ", Furigana "b" "1"])
                               (map lex2group_ ["a___b[1]"
                                               ,"a____b[1]"
                                               ,"a____________b[1]"
                                               ,"a_______________________b[1]"
                                               ,"a_____________________________________________________b[1]"
                                               ])

test4_label = "_ is printed if escaped by §"
test4 :: Test
test4 = TestCase $ assertEqual "for (\"a§__b[1§_1]§_§_c[2]\")"
                               [Kana "a_", Furigana "b" "1_1", Furigana "__c" "2"]
                               (lex2group_ "a§__b[1§_1]§_§_c[2]")
      

test5_label = "§ needs not be escaped, multiple underscores, multiple §"
test5 :: Test
test5 = TestCase $ assertEqual "for (\"§ §_  §§ §§_ §_§ §_§_  §§§ §§§_ §§_§ §§_§_ §_§§ §_§§_ §_§_§ §_§_§_\")"
                               [Kana "§",Kana " ",Kana "_",Kana " ",Kana "§§",Kana " ",Kana "§_",Kana " ",Kana "_§",Kana " ",Kana "__",Kana " ",Kana "§§§",Kana " ",Kana "§§_",Kana " ",Kana "§_§",Kana " ",Kana "§__",Kana " ",Kana "_§§",Kana " ",Kana "_§_",Kana " ",Kana "__§",Kana " ",Kana "___"]
                               (lex2group_ "§ §_  §§ §§_ §_§ §_§_  §§§ §§§_ §§_§ §§_§_ §_§§ §_§§_ §_§_§ §_§_§_")
      
