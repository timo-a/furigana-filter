#unit tests
stack ghc --ghc-package-path ../SimpleAnkiParser.hs TestAnki_.hs
./TestAnki_.hs  


#integration test
cat test_ | while read line; do echo $line | pandoc -f markdown -t html --filter anki_ >> test_results; done

diff test_results ref_
